import { Application, HttpServerStd } from './deps.ts'

import router from './routes.ts'

const HOST = '0.0.0.0'
const API_PORT = 10302


const app = new Application({
     serverConstructor: HttpServerStd
})
app.use(router.routes())
app.use(router.allowedMethods())

console.log(`Listening on port ${API_PORT} ...`)
await app.listen(`${HOST}:${API_PORT}`)
