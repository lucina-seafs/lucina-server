import { Router, Context } from './deps.ts'

import { register, login, logout, getUser } from './controllers/usersController.ts'
import { getNearUsersCount, sendLocation } from './controllers/locationController.ts'
import { getMessages, wsMessagesConnect } from './controllers/messagesController.ts'
import { authent } from './middlewares/logger.ts';


import {
  acceptWebSocket,
  isWebSocketCloseEvent,
  isWebSocketPingEvent,
  WebSocket,
  acceptable
} from "https://deno.land/std/ws/mod.ts";
import db from './db/database.ts'

    
const router = new Router()
router//.post('/register', register)
      //users
      .post('/login', login)
      .post('/logout', authent, logout)
      .get('/user', authent, getUser)
      // location
      .get('/county/:county', authent, getNearUsersCount)
      .post('/county', authent, sendLocation)
      // messages
      .get('/ws/messages/:county', authent, wsMessagesConnect)
      .get('/messages/:county', authent, getMessages)

      
export default router


