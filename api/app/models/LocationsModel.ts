import db from '../db/database.ts'
import { Model, DataTypes } from 'https://deno.land/x/denodb/mod.ts';


class Loc extends Model{
    static table = 'locations';

    static fields = {
        id: { primaryKey: true, autoIncrement: true },
        county: DataTypes.TEXT
    }
    
     static defaults = {}
}


db.link([Loc])
export default Loc
