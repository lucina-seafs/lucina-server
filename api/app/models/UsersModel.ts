import db from '../db/database.ts'
import { Model, DataTypes } from 'https://deno.land/x/denodb/mod.ts'
import { cryptoRandomString } from "https://deno.land/x/crypto_random_string@1.0.0/mod.ts"
import { lowerCase } from "https://deno.land/x/case/mod.ts"


class User extends Model{
    
    static table = 'users';

    static fields = {
        id: { primaryKey: true, autoIncrement: true },
        name: DataTypes.TEXT,
        password: DataTypes.TEXT,
        token: DataTypes.TEXT,
        mail: DataTypes.TEXT,
        role_id: DataTypes.INTEGER,
        location_id: DataTypes.INTEGER,
        nb_likes: DataTypes.INTEGER,
        nb_reports: DataTypes.INTEGER
    }

    static defaults = {
        token: null,
        role_id: 1,
        location_id: 1,
        nb_likes: 0,
        nb_reports: 0,
    }
    
    static async login(email: string, pass: string){
        var tok = ""
        email = lowerCase(String(email))
        try{
            console.log(email, pass)
            var res = await User.where({ mail: email, password: pass }).first()
            if (res.id != ""){
                // generate token to return it
                tok = User.generateToken(lowerCase(String(res.name)))
                await User.where({ mail: email, password: pass }).update({token: tok})
            }else{
                return ""
            }
        }catch(e){
            console.log(e)
        }
        return tok
    }
    
    static async logout(tok: string){
        // TODO: remove location + token
        try{
            var res = User.where({ token: tok }).update({ token: null, location_id: 1 })
        }catch(e){
            console.log(e)
        }
        return true
    }
    
    static async validToken(token: string){
        var valid = false
        if (token != null){
            var res = await User.where('token', token).get()
            if (res.length != 0){
                valid = true
            }
        }
        return valid
    }
    
    static generateToken(name: string){
        return name + "@" + cryptoRandomString({length: 20})    
    }
    
    static async getUser(token: string){
        var user = await User.select("mail", "role_id", "location_id", "nb_likes", "nb_reports", "name").where("token", token).first()
        return user
    }
}


db.link([User])
export default User
