 import db from '../db/database.ts'
import { Model, DataTypes } from 'https://deno.land/x/denodb/mod.ts';


class Message extends Model{
    static table = 'messages';

    static fields = {
        id: { primaryKey: true, autoIncrement: true },
        location_id: DataTypes.INTEGER,
        author_id: DataTypes.INTEGER,
        content: DataTypes.TEXT,
        nb_likes: DataTypes.INTEGER,
        nb_reports: DataTypes.INTEGER,
        posting_date: DataTypes.DATE
    }
    
     static defaults = {
         nb_likes: 0,
         nb_reports: 0
    }
}


db.link([Message])
export default Message
