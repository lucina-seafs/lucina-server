import { Database, PostgresConnector } from 'https://deno.land/x/denodb/mod.ts';

const connection = new PostgresConnector({
  database: 'lucina_deno',
  host: 'postgres_lucina',
  username: 'postgres',
  password: 'lucina_db_pass',
  port: 5432
});

const db = await new Database(connection)

export default db
