import { Context }from '../deps.ts'
import User from '../models/UsersModel.ts'

// test_request : curl -X POST -H 'Content-Type: application/json' -d '{"mail": "setsukooseki@gmail.com", "pass": "root"}' 0.0.0.0:10302/login
const login = async (ctx: any) => {
    const body = await ctx.request.body()["value"]
    const mail = body.mail
    const pass = body.pass
    console.log(body)
    
    // TODO : hash the pass

    const newToken = await User.login(mail, pass)
    console.log("token : " + newToken)
    if (newToken != ""){
        console.log("[INFO] User " + mail + " connected")
        ctx.response.body = newToken
    }else{
        console.log("[INFO] User " + mail + " failed connexion")
        ctx.response.body = "[FAILED] User " + mail + " failed connexion"
    }
}

// test_request : curl -X POST -H 'Token: <token>'  0.0.0.0:10302/logout
const logout = async (ctx: any) => {
    const headers = await ctx.request.headers
    const token = headers.get("token")

    var loggedOut = false

    if (token != null){
        loggedOut = await User.logout(token)
    }
    if (loggedOut !== undefined){
        console.log("[INFO] User " + token + " disconnected")
        
        ctx.response.body = "[SUCCESS] User " + token + "disconnected"
    }else{
        console.log("[INFO] A user failed disconnection")
        ctx.response.body = "FAILED"
    }
}

// test_request : curl -X POST -H 'Content-Type: application/json' -d '{"name": "Ryptix", "pass": "root", "mail": "test@test.com"}' 0.0.0.0:10302/register
const register = async (ctx: any) => {
    const body = await ctx.request.body()["value"]
    
    //TODO : creds verifications + client side
    // password > 8 chars
    // no special chars
    // no null
    try{
        User.create({ name: body.name, password: body.pass, mail: body.mail })
    }catch(e){
        console.log(e) 
    }
    return true
}

const getUser = async (ctx: any) => {
    const headers = await ctx.request.headers
    const token = headers.get("token")
    var user = null
    
    if (token != null){
        user = await User.getUser(token)
        console.log("name = " + user.name)
    }
    ctx.response.body = user
}

export { register, login, logout, getUser}
