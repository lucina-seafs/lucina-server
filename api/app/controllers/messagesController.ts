import Message from '../models/MessagesModel.ts'
import Loc from '../models/LocationsModel.ts'
import User from '../models/UsersModel.ts'

import { lowerCase } from "https://deno.land/x/case/mod.ts"
import { Context } from '../deps.ts'
import { getQuery } from 'https://deno.land/x/oak/helpers.ts'
import { acceptWebSocket, isWebSocketCloseEvent, acceptable } from '../deps.ts'
import { v4 } from 'https://deno.land/std@0.51.0/uuid/mod.ts'
import { WebSocketClient, WebSocketServer, StandardWebSocketClient } from "https://deno.land/x/websocket/mod.ts";
import { encodeUrl } from "https://deno.land/x/encodeurl/mod.ts";

let wsUserList: WebSocket[] = Array()

// test_request : curl -X GET -H 'Token: <token>' 0.0.0.0:10302/messages/Rennes
const getMessages = async (ctx: any) => {
    console.log("fetch messages")
    const county = getQuery(ctx, { mergeParams: true })['county']
    console.log("get msg from ", county)
    // Send message to DB

    const id_loc = await Loc.select("id").where("county", encodeUrl(lowerCase(String(county)))).first()

    console.log(id_loc)
    
    
    // Wanted format : name: <author_name>, content: <content>, nb_likes, nb_reports, posting_date
    const messages = await Message.select("name", "content", Message.field("nb_likes"), Message.field("nb_reports"), "posting_date")
                    .where(User.field("location_id"), String(id_loc.id))
                    .join(User, User.field("id"), Message.field("author_id"))
                    .get()
    console.log(messages)
    ctx.response.body = messages
}


// test_request : curl -X GET -H 'Upgrade: websocket' -H 'connection: "Upgrade"' -H 'Sec-WebSocket-Key: test' -H 'Token: <token>' 0.0.0.0:10302/ws/messages/Rennes
const wsMessagesConnect =  async (ctx: any) => {
    // upgrade to websocket     
    try{
        if (acceptable(ctx.request)){
            const sock = await ctx.upgrade()
            handleWs(sock, ctx)
        }else{
            console.error(`failed to accept websocket` + acceptable(ctx.request));
        }
    }catch(e){
        console.log(e)
    }
}

async function handleWs(ws: WebSocket, ctx: any) {
    console.log(ctx.request)
    console.log("socket connected! ", ws)
    ws.send("test opened")
    ws.addEventListener('open', (event) => {
        console.log("server opened")
    })
    try {
        ws.onopen = () => {
            console.log("message WebSocket opened", getQuery(ctx, { mergeParams: true })['county'])
            wsUserList.push(ws)
        }

        ws.onmessage = (msg) => {
            console.log("message received : ", msg)
            //TODO: add message to DB
            sendMessage(ctx, msg.data)
            console.log("data was added, sending now")
            for (let userWs of wsUserList){
                userWs.send(msg.data)
            }
        }


    } catch (e) {
    console.error(`failed to receive frame: ${e}`);
    }
    if (!ws.CLOSED) {
        try{
        await ws.close(1000)
        }catch(e){
        console.log("[ERROR] Can't close message WebSocket : ", e)
        }
    }
}


// msg_template : {"name":"Sonata","content":"Henlo","nb_likes":0,"nb_reports":0,"posting_date":"Sep 6, 2021 17:06:20"}
async function sendMessage(ctx: any, msg: any) {
    const json_msg = JSON.parse(msg)
    console.log(json_msg)
        
    const county = getQuery(ctx, { mergeParams: true })['county']
    const author = json_msg["name"]
    
    console.log("county = ", county)

    const id_loc = await Loc.select("id").where("county", encodeUrl(lowerCase(String(county)))).first()
    const id_auth = await User.select("id").where("name", author).first()

    const test = await Message.create({ location_id: String(id_loc.id),
                        author_id: String(id_auth.id),
                        content: json_msg["content"],
                        posting_date: json_msg["posting_date"]
    })
}

export { getMessages, wsMessagesConnect }
