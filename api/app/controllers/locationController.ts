import { Context } from '../deps.ts'
import User from '../models/UsersModel.ts'
import Loc from '../models/LocationsModel.ts'
import { lowerCase } from "https://deno.land/x/case/mod.ts"
import { encodeUrl } from "https://deno.land/x/encodeurl/mod.ts";
import { getQuery } from 'https://deno.land/x/oak/helpers.ts'


// test_request : curl -X POST -H 'Token: <token>' -H 'Content-Type: application/json' -d '{"county": "rennes"}' 0.0.0.0:10302/county
const sendLocation = async (ctx: any) => {
    const body = await ctx.request.body()["value"]
    const count = encodeUrl(lowerCase(body.county))
    const headers = await ctx.request.headers
    const token = headers.get("token")
    // serach for location id
    var id_loc = await Loc.where({ county: count }).select("id").first()

    if (id_loc == undefined){
        // add location into db and return id
        try{
            await Loc.create({ county: count })
            id_loc = await Loc.where({ county: count }).select("id").first()
        }catch(e){
            console.log(e)
        }
    }
    // put correct location into user tab
    await User.where("token", token).update("location_id", String(id_loc.id) )    
    console.log("Location" + count + " sent")
    // successful return
    ctx.response.body = "[SUCCESS] Location sent"
}

// test_request : curl -X GET -H 'Token: <token>' 0.0.0.0:10302/county/Rennes
const getNearUsersCount  = async (ctx: any) => {
    // count users with same given location
    const body = await ctx.request.body()["value"]
    const county = encodeUrl(lowerCase(getQuery(ctx, { mergeParams: true })['county']))
    console.log(county)
    const id_loc = await Loc.select("id").where("county", county).first()
    const nbUsers = await User.where("location_id", String(id_loc.id)).count() - 1
    console.log("ask number of users in " + county)
    ctx.response.body = nbUsers
}

export { getNearUsersCount, sendLocation }
