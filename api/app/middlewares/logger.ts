import User from '../models/UsersModel.ts'
import { Context }from 'https://deno.land/x/oak/mod.ts'


const authent = async (ctx: any, next: Function) => {
    const headers = await ctx.request.headers
    const token = headers.get("token")

    
    
    var validToken = true
    if (token != null){
        validToken = await User.validToken(token)
    }
    if (validToken){
        await next();
    }else{
        ctx.response.body = "[WARNING] Someone tried to access the API without a valid token :" + validToken
    }
}

export { authent }
